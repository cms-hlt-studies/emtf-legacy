/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.l1t.emtf.tools.automats.ugt_rate_vs_pu;

import ch.cern.cms.l1t.emtf.tools.EMTFTools;
import ch.cern.cms.l1t.emtf.tools.common.DBScanParameters;
import ch.cern.cms.l1t.emtf.tools.common.fitters.Fit;
import ch.cern.cms.l1t.emtf.tools.common.fitters.Fitter;
import static ch.cern.cms.spartan.core.SpartanLang.*;
import ch.cern.cms.spartan.tools.concurrent.dispatchers.RequestDispatcher;
import ch.cern.cms.spartan.tools.concurrent.dispatchers.RequestDispatcher.SharedContext;
import ch.cern.cms.spartan.tools.database.connections.ConnectionShield;
import ch.cern.cms.spartan.tools.database.resultsets.ResultSetStreamTuple;
import ch.cern.cms.spartan.tools.database.snippets.TemplateSnippet;
import ch.cern.cms.spartan.tools.database.statements.PreparedStatementShield;
import ch.cern.cms.spartan.tools.database.statements.StatementTemplate;
import ch.cern.cms.spartan.tools.threading.pool.Loan;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.Clusterable;
import org.apache.commons.math3.ml.clustering.DBSCANClusterer;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

/**
 *
 * @author omiguelc
 */
public class UGTRateVsPUFitterAutomat extends RequestDispatcher {

	public static enum Field {
		RATE, NORM_RATE, XSEC;
	}

	private final String output_file_name;

	private final Field field;
	private final Fitter fitter;
	private final DBScanParameters dbscan_parameters;

	public UGTRateVsPUFitterAutomat(String output_file_name, Field field, Fitter fitter, DBScanParameters dbscan_parameters) {
		this.output_file_name = output_file_name;

		this.field = field;
		this.fitter = fitter;
		this.dbscan_parameters = dbscan_parameters;
	}

	@Override
	protected void handle(int worker_id, Map request, SharedContext shared_context) {
		try (Loan<ConnectionShield> promise = connection("CMS_RPC_R@adg")) {
			// PREPARE STATEMENT
			StatementTemplate statement_template = getStatementTemplate("ugt_rate_vs_pu::select");
			statement_template.putSnippet("runnumbers", new TemplateSnippet((String) request.get("RUNNUMBERS"), null, null));
			PreparedStatementShield statement = statement_template.prepare();

			// RUN STATEMENT
			ResultSetStreamTuple<Map> stream_tuple = statement.queryAndStreamMaps(
					promise.value(), request
			);

			// GET GOOD POINTS
			List<double[]> points = getGoodPoints(request, stream_tuple.getStream());

			// RELEASE CONNECTION
			promise.release();

			// VALIDATE STREAM WAS SUCCESSFUL
			stream_tuple.getExceptionAnchor().throwIfPresent();

			// PROCESS
			if (points.size() > 75) {
				// FIT
				AnalysisTupple analysis_tuple = analyze(points);

				if (analysis_tuple == null) {
					return;
				}

				// UNPACK
				Fit fit = analysis_tuple.getFit();
				SummaryStatistics stats = analysis_tuple.getFieldStatistics();

				// CALCULATE GROWTH
				double y_at_1 = fit.predict(1);
				double y_at_50 = fit.predict(50);
				double growth = y_at_1 != 0 ? (y_at_50 / y_at_1) - 1d : Double.NaN;

				// GET PRINT WRITTER
				PrintWriter fit_pw;

				synchronized (shared_context.mutex("output_pw")) {
					fit_pw = shared_context.getResource("output_pw");

					if (fit_pw == null) {
						try {
							String directory_path = EMTFTools.OUTPUT_DIRECTORY + "/ugt_rate_vs_pu/" + field.name().toLowerCase() + "/summaries/";
							File directory = new File(directory_path);
							directory.mkdirs();

							fit_pw = new PrintWriter(new File(String.format(
									"%s/%s.csv",
									directory_path,
									output_file_name
							)));

							shared_context.putResource("output_pw", fit_pw);
						} catch (FileNotFoundException ex) {
							Logger.getLogger(UGTRateVsPUFitterAutomat.class.getName()).log(Level.SEVERE, null, ex);
						}
					}
				}

				// WRITE RESULTS
				fit_pw.println(String.format(
						"\"%s\",\"%s\",%d,\"%s\",\"%s\",%d,%d,\"%s\",%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%d,%.16f,%.16f",
						request.get("RUNTIME_TYPE"),
						request.get("ALGO"),
						request.get("LHCFILL"),
						request.get("STARTTIME"),
						request.get("STOPTIME"),
						request.get("STARTTIME_IN_UNIXTIME"),
						request.get("STOPTIME_IN_UNIXTIME"),
						request.get("INJECTIONSCHEME"),
						request.get("ENERGY"),
						request.get("PEAKPILEUP"),
						request.get("PEAKLUMI"),
						request.get("CROSSINGANGLE"),
						request.get("NCOLLIDINGBUNCHES"),
						growth,
						fit.getParameter(0),
						fit.getParameterError(0),
						fit.getParameter(1),
						fit.getParameterError(1),
						fit.getParameter(2),
						fit.getParameterError(2),
						fit.getChiSqr(),
						fit.getN(),
						stats.getMin(),
						stats.getMax()
				));
			}
		} catch (SQLException ex) {
			Logger.getLogger(UGTRateVsPUFitterAutomat.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	protected void done() {
		// Close the print writter
		PrintWriter fit_pw = getSharedContext().getResource("output_pw");

		if (fit_pw != null) {
			fit_pw.close();
		}
	}

	private List<double[]> getGoodPoints(Map request, Stream<Map> records) {
		// Buffer records
		DescriptiveStatistics pu_stats = new DescriptiveStatistics();
		DescriptiveStatistics field_stats = new DescriptiveStatistics();
		List<Map> record_buffer = new ArrayList();

		records.filter((record) -> {
			return record.get("PILEUP") != null && record.get("RATE") != null && record.get("XSEC") != null;
		})
				.map(record -> (Map) new HashMap(record))
				.forEach((Map record) -> {
					if (Field.NORM_RATE.equals(field)) {
						double value = (Double) record.get(Field.RATE.name());
						value = value / (double) request.get("NCOLLIDINGBUNCHES");
						record.put(Field.NORM_RATE.name(), value);
					}

					pu_stats.addValue((double) record.get("PILEUP"));
					field_stats.addValue((double) record.get(field.name()));
					record_buffer.add(record);
				});

		if (record_buffer.size() > 100) {
			// MEDIANS
			double median_pile_up = pu_stats.getPercentile(50);
			double median_field_value = field_stats.getPercentile(50);

			// MAKE POINTS
			List<PointWrapper> points = new ArrayList();

			record_buffer.forEach(record -> {
				double pile_up = (double) record.get("PILEUP");
				double field_value = (double) record.get(field.name());

				double x = pile_up / median_pile_up;
				double y = field_value / median_field_value;

				points.add(new PointWrapper(
						record,
						new double[]{x, y}
				));
			});

			// DBSCAN
			List<double[]> good_points = new ArrayList();

			DBSCANClusterer dbscan = new DBSCANClusterer(dbscan_parameters.getEps(), dbscan_parameters.getMinPts());
			List<Cluster<PointWrapper>> cluster_results = dbscan.cluster(points);

			for (int cluster_id = 0; cluster_id < cluster_results.size(); cluster_id++) {
				for (PointWrapper wrapper : cluster_results.get(cluster_id).getPoints()) {
					good_points.add(wrapper.getNTuple());
				}
			}

			return good_points;
		}

		return Collections.emptyList();
	}

	private synchronized AnalysisTupple analyze(List<double[]> points) {
		// FIT
		Fit fit = fitter.fit(points);

		if (fit == null) {
			return null;
		}

		// STATS
		SummaryStatistics field_statistics = new SummaryStatistics();

		points.forEach((point) -> {
			field_statistics.addValue(point[1]);
		});

		// RETURN
		return new AnalysisTupple(fit, field_statistics);
	}

	private class PointWrapper implements Clusterable {

		private final Map record;
		private final double[] point;

		public PointWrapper(Map record, double[] point) {
			this.record = record;
			this.point = point;
		}

		public Map getRecord() {
			return record;
		}

		public double[] getNTuple() {
			return new double[]{
				(double) record.get("PILEUP"),
				(double) record.get(field.name())
			};
		}

		@Override
		public double[] getPoint() {
			return point;
		}
	}

	private class AnalysisTupple {

		private final Fit fit;
		private final SummaryStatistics field_statistics;

		public AnalysisTupple(Fit fit, SummaryStatistics field_statistics) {
			this.fit = fit;
			this.zstatistics = field_statistics;
		}

		public Fit getFit() {
			return fit;
		}

		public SummaryStatistics getFieldStatistics() {
			return field_statistics;
		}
	}
}
