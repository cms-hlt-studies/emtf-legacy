/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.l1t.emtf.tools.automats.collision_run_list;

import ch.cern.cms.spartan.commons.database.connections.ConnectionShield;
import ch.cern.cms.spartan.commons.lang.pointers.Pointer;
import ch.cern.cms.spartan.commons.threading.pool.Loan;
import ch.cern.cms.spartan.core.SpartanLang;
import static ch.cern.cms.spartan.core.SpartanLang.*;
import ch.cern.cms.spartan.tools.concurrent.dispatchers.RequestDispatcher;
import ch.cern.cms.spartan.tools.concurrent.dispatchers.RequestDispatcher.SharedContext;
import ch.cern.cms.spartan.util.json.JSONUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONObject;

/**
 *
 * @author omiguelc
 */
public class CollisionRunListAutomat extends RequestDispatcher {

	private Map dcs;
	private Map golden;

	public CollisionRunListAutomat() {
		dcs = getLSCounts("https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions18/13TeV/DCSOnly/json_DCSONLY.txt");
		golden = getLSCounts("https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions18/13TeV/PromptReco/Cert_314472-325175_13TeV_PromptReco_Collisions18_JSON.txt");

		try {
			String out_directory_path = "/home/omiguelc/Desktop/emtf_test";
			String directory_path = out_directory_path + "/collision_run_list";
			File directory = new File(directory_path);
			directory.mkdirs();

			String output_file = String.format("%s/%s.csv", directory_path, "out");
			PrintWriter pw = new PrintWriter(new File(output_file));

			pw.println(String.format(
					"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
					"ERA", "FILL", "RUN", "nBunch", "Total LS", "Phys LS", "DCS LS", "Gold LS", "L1muon", "L1calo", "RR Comments"
			));

			getSharedContext().putResource("writer", pw);
		} catch (FileNotFoundException ex) {
			Logger.getLogger(CollisionRunListAutomat.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	protected void handle(int worker_id, Map request, SharedContext shared_context) {
		try (Loan<ConnectionShield> loan = connection("CMS_RPC_R@adg")) {
			int nbunches = (int) SpartanLang.runQueryStatementAndGetFirstValue(
					"collision_run_list::fill_nbunches",
					loan.value(), request
			).orElse(-1);
			int ls_count = (int) SpartanLang.runQueryStatementAndGetFirstValue(
					"collision_run_list::run_ls_count",
					loan.value(), request
			).orElse(-1);
			int dcs_ls_count = (int) dcs.getOrDefault(request.get("RUN"), -1);
			int golden_ls_count = (int) golden.getOrDefault(request.get("RUN"), -1);

			PrintWriter pw = shared_context.getResource("writer");
			pw.println(String.format(
					"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
					request.get("ERA"),
					request.get("FILL"),
					request.get("RUN"),
					nbunches,
					ls_count,
					request.get("Phys LS"),
					dcs_ls_count,
					golden_ls_count,
					request.get("L1muon"),
					request.get("L1calo"),
					request.get("RR Comments")
			));
		} catch (SQLException ex) {
			Logger.getLogger(CollisionRunListAutomat.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	protected void done() {
		PrintWriter pw = getSharedContext().getResource("writer");
		pw.close();
	}

	private Map getLSCounts(String web_file) {
		try {
			URL url = new URL(web_file);
			HttpsURLConnection url_connection = (HttpsURLConnection) url.openConnection(); // Cast shouldn't fail
			HttpsURLConnection.setFollowRedirects(true);

			InputStream input_stream = url_connection.getInputStream();
			JSONObject json = JSONUtils.readJSON(input_stream);
			Map obj = JSONUtils.javafy(json);

			Map ls_counts = new HashMap();

			obj.forEach((runnumber, ls_pairs) -> {
				Pointer<Integer> count = new Pointer(0);

				((List) ls_pairs).forEach(ls_pair -> {
					int from_ls = (int) ((List) ls_pair).get(0);
					int to_ls = (int) ((List) ls_pair).get(1);
					count.set(count.value() + (to_ls - from_ls + 1));
				});

				ls_counts.put(runnumber, count.value());
			});

			return Collections.unmodifiableMap(ls_counts);
		} catch (MalformedURLException ex) {
			Logger.getLogger(CollisionRunListAutomat.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(CollisionRunListAutomat.class.getName()).log(Level.SEVERE, null, ex);
		}

		return Collections.emptyMap();
	}

}
