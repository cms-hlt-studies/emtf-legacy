@Register ({
	"name": "high_pu_ls::select",

	"fields": {
		"LUMISECTION": "integer"
	},

	"parameters": {
		"RUNNUMBER": "integer",
		"FROM_LS": "integer",
		"TO_LS": "integer"
	}
})

@Template
SELECT
	LUMISECTION
FROM
	CMS_RUNTIME_LOGGER.LUMI_SECTIONS
WHERE PILEUP > 50 AND RUNNUMBER=:RUNNUMBER AND LUMISECTION BETWEEN :FROM_LS AND :TO_LS