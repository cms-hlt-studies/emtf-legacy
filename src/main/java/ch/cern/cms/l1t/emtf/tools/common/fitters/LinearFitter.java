/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.l1t.emtf.tools.common.fitters;

import java.util.List;
import org.apache.commons.math3.stat.regression.SimpleRegression;

/**
 *
 * @author omiguelc
 */
public class LinearFitter implements Fitter {

	@Override
	public Fit fit(List<double[]> points) {
		SimpleRegression regression = new SimpleRegression();

		points.forEach((point) -> {
			regression.addData(point[0], point[1]);
		});

		double chi_sqr_ptr[] = {0};
		points.forEach(point -> {
			double pred = regression.predict(point[0]);
			double diff = point[1] - pred;

			chi_sqr_ptr[0] += (diff * diff) / pred;
		});

		double[] parameter_values = {-1, regression.getIntercept(), regression.getSlope()};
		double[] parameter_errors = {-1, regression.getInterceptStdErr(), regression.getSlopeStdErr()};

		return new Fit() {
			@Override
			public long getN() {
				return regression.getN();
			}

			@Override
			public double getParameter(int i) {
				return parameter_values[i];
			}

			@Override
			public double getParameterError(int i) {
				return parameter_errors[i];
			}

			@Override
			public double getChiSqr() {
				return chi_sqr_ptr[0];
			}

			@Override
			public double predict(double x) {
				return regression.predict(x);
			}
		};
	}

}
