/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.l1t.emtf.tools.automats.ugt_rate_vs_pu;

import ch.cern.cms.l1t.emtf.tools.EMTFTools;
import ch.cern.cms.l1t.emtf.tools.common.DBScanParameters;
import static ch.cern.cms.spartan.core.SpartanLang.*;
import ch.cern.cms.spartan.tools.concurrent.dispatchers.RequestDispatcher;
import ch.cern.cms.spartan.tools.concurrent.dispatchers.RequestDispatcher.SharedContext;
import ch.cern.cms.spartan.tools.database.connections.ConnectionShield;
import ch.cern.cms.spartan.tools.database.resultsets.ResultSetStreamTuple;
import ch.cern.cms.spartan.tools.database.snippets.TemplateSnippet;
import ch.cern.cms.spartan.tools.database.statements.PreparedStatementShield;
import ch.cern.cms.spartan.tools.database.statements.StatementTemplate;
import ch.cern.cms.spartan.tools.threading.pool.Loan;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.Clusterable;
import org.apache.commons.math3.ml.clustering.DBSCANClusterer;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

/**
 *
 * @author omiguelc
 */
public class UGTRateVsPUDBScanAutomat extends RequestDispatcher {

	// STATIC
	public static enum Field {
		RATE, NORM_RATE, XSEC;
	}

	// MEMBER
	private final Field field;
	private final DBScanParameters dbscan_parameters;

	public UGTRateVsPUDBScanAutomat(Field field, DBScanParameters dbscan_parameters) {
		this.field = field;
		this.dbscan_parameters = dbscan_parameters;
	}

	@Override
	protected void handle(int worker_id, Map request, SharedContext shared_context) {
		try (Loan<ConnectionShield> promise = connection("CMS_RPC_R@adg")) {
			// PREPARE STATEMENT
			StatementTemplate statement_template = getStatementTemplate("ugt_rate_vs_pu::select");
			statement_template.putSnippet("runnumbers", new TemplateSnippet((String) request.get("RUNNUMBERS"), null, null));
			PreparedStatementShield statement = statement_template.prepare();

			// RUN STATEMENT
			ResultSetStreamTuple<Map> stream_tuple = statement.queryAndStreamMaps(
					promise.value(), request
			);

			// BUFFER RECORDS
			DescriptiveStatistics pu_stats = new DescriptiveStatistics();
			DescriptiveStatistics field_stats = new DescriptiveStatistics();
			List<Map> record_buffer = new ArrayList();

			stream_tuple.getStream()
					.filter((record) -> {
						return record.get("PILEUP") != null && record.get("RATE") != null && record.get("XSEC") != null;
					})
					.map(record -> (Map) new HashMap(record))
					.forEach((Map record) -> {
						if (Field.NORM_RATE.equals(field)) {
							double value = (Double) record.get(Field.RATE.name());
							value = value / (double) request.get("NCOLLIDINGBUNCHES");
							record.put(Field.NORM_RATE.name(), value);
						}

						pu_stats.addValue((double) record.get("PILEUP"));
						field_stats.addValue((double) record.get(field.name()));
						record_buffer.add(record);
					});

			// RELEASE CONNECTION
			promise.release();

			// VALIDATE STREAM WAS SUCCESSFUL
			stream_tuple.getExceptionAnchor().throwIfPresent();

			// PROCESS
			if (record_buffer.size() > 75) {
				// MEDIANS
				double median_pile_up = pu_stats.getPercentile(50);
				double median_field_value = field_stats.getPercentile(50);

				// MAKE POINTS
				List<PointWrapper> points = new ArrayList();

				record_buffer.forEach(record -> {
					double pile_up = (double) record.get("PILEUP");
					double field_value = (double) record.get(field.name());

					double x = pile_up / median_pile_up;
					double y = field_value / median_field_value;

					record.put("X", x);
					record.put("Y", y);

					points.add(new PointWrapper(
							record,
							new double[]{x, y}
					));
				});

				// DBSCAN
				double eps = dbscan_parameters.getEps();
				int min_pts = dbscan_parameters.getMinPts();

				DBSCANClusterer dbscan = new DBSCANClusterer(eps, min_pts);
				List<Cluster<PointWrapper>> cluster_results = dbscan.cluster(points);

				for (int cluster_id = 0; cluster_id < cluster_results.size(); cluster_id++) {
					for (PointWrapper point : cluster_results.get(cluster_id).getPoints()) {
						point.getRecord().put("CLUSTER", cluster_id + 1);
					}
				}

				// OUTPUT TO FILE
				String algo = (String) request.get("ALGO");
				int lhcfill = (int) request.get("LHCFILL");

				String out_directory_path = EMTFTools.OUTPUT_DIRECTORY + "/ugt_rate_vs_pu/" + field.name().toLowerCase() + "/data";
				String directory_path = out_directory_path + "/" + algo;
				File directory = new File(directory_path);
				directory.mkdirs();

				String output_file = String.format("%s/%d.csv", directory_path, lhcfill);

				try (PrintWriter fit_pw = new PrintWriter(new File(output_file))) {
					record_buffer.forEach((record) -> {
						fit_pw.println(String.format(
								"%.16f,%.16f,%d",
								record.get("PILEUP"),
								record.get(field.name()),
								record.getOrDefault("CLUSTER", 0)
						));
					});
				}
			}
		} catch (FileNotFoundException | SQLException ex) {
			Logger.getLogger(UGTRateVsPUDBScanAutomat.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private static class PointWrapper implements Clusterable {

		private final Map record;
		private final double[] point;

		public PointWrapper(Map record, double[] point) {
			this.record = record;
			this.point = point;
		}

		public Map getRecord() {
			return record;
		}

		@Override
		public double[] getPoint() {
			return point;
		}
	}

}
