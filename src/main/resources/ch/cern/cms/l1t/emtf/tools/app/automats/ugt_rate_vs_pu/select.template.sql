@Register ({
	"name": "ugt_rate_vs_pu::select",

	"fields": {
		"PILEUP": "double",
		"RATE": "double",
		"XSEC": "double"
	},

	"parameters": {
		"ALGO_INDEX": "integer"
	}
})

@Template
SELECT LUMI_SECTIONS.PILEUP, ALGO.ALGO_RATE RATE, ALGO.ALGO_RATE / LUMI_SECTIONS.INSTLUMI XSEC FROM
	{% ugt_algo_scalers %} ALGO
INNER JOIN
	{% ugt_lumi_sections %} LUMI
ON LUMI.ID = ALGO.LUMI_SECTIONS_ID
AND ALGO.SCALER_TYPE=1  AND ALGO.ALGO_RATE > 0
INNER JOIN
	{% ugt_run_prescale %} PRESCALE
ON PRESCALE.RUN_NUMBER = LUMI.RUN_NUMBER AND PRESCALE.ALGO_INDEX=ALGO.ALGO_INDEX
AND PRESCALE.PRESCALE_INDEX=LUMI.PRESCALE_INDEX
INNER JOIN
	{% lumi_sections %} LUMI_SECTIONS
ON LUMI_SECTIONS.RUNNUMBER=LUMI.RUN_NUMBER AND LUMI_SECTIONS.LUMISECTION=LUMI.LUMI_SECTION
AND (LUMI_SECTIONS.BEAM1_PRESENT = 1 AND LUMI_SECTIONS.BEAM2_PRESENT = 1)
AND (LUMI_SECTIONS.BEAM1_STABLE = 1 AND LUMI_SECTIONS.BEAM2_STABLE = 1)
AND LUMI_SECTIONS.DELIVLUMI > 0 AND LUMI_SECTIONS.INSTLUMI > 0
AND LUMI_SECTIONS.DELIVLUMI != LUMI_SECTIONS.INSTLUMI
AND LUMI_SECTIONS.DELIVLUMISECTION > 0
AND LUMI_SECTIONS.PILEUP > 10
AND LUMI_SECTIONS.EBP_READY=1 AND LUMI_SECTIONS.EBM_READY=1
AND LUMI_SECTIONS.EEP_READY=1 AND LUMI_SECTIONS.EEM_READY=1
AND LUMI_SECTIONS.HBHEA_READY=1 AND LUMI_SECTIONS.HBHEB_READY=1 AND LUMI_SECTIONS.HBHEC_READY=1
AND LUMI_SECTIONS.HF_READY=1 AND LUMI_SECTIONS.HO_READY=1
AND LUMI_SECTIONS.RPC_READY=1
AND LUMI_SECTIONS.DT0_READY=1 AND LUMI_SECTIONS.DTP_READY=1 AND LUMI_SECTIONS.DTM_READY=1
AND LUMI_SECTIONS.CSCP_READY=1 AND LUMI_SECTIONS.CSCM_READY=1
AND LUMI_SECTIONS.CMS_ACTIVE=1 AND LUMI_SECTIONS.PHYSICS_FLAG=1
WHERE LUMI.RUN_NUMBER IN ({% runnumbers %}) AND ALGO.ALGO_INDEX=:ALGO_INDEX
ORDER BY LUMI_SECTIONS.PILEUP