/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.l1t.emtf.tools.automats.high_pu_lumisections;

import static ch.cern.cms.spartan.core.SpartanLang.*;
import ch.cern.cms.spartan.tools.concurrent.dispatchers.RequestDispatcher;
import ch.cern.cms.spartan.tools.concurrent.dispatchers.RequestDispatcher.SharedContext;
import ch.cern.cms.spartan.tools.database.connections.ConnectionShield;
import ch.cern.cms.spartan.tools.database.resultsets.ResultSetStreamTuple;
import ch.cern.cms.spartan.tools.threading.pool.Loan;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author omiguelc
 */
public class HighPULSAutomat extends RequestDispatcher {

	private final Map runnumbers;

	public HighPULSAutomat() {
		runnumbers = new HashMap();
	}

	@Override
	protected void handle(int worker_id, Map request, SharedContext shared_context) {
		try (Loan<ConnectionShield> promise = connection("CMS_RPC_R@adg")) {
			// GET REQUEST ARGS
			String arg_runnumber = (String) request.get("RUNNUMBER");

			// BUILD INTIAL INTERVAL
			List<List> ls_intervals = new ArrayList();
			int ls_interval[] = {-1, -1};

			// RUN STATEMENT
			ResultSetStreamTuple<Map> stream_tuple = runQueryStatementAndStreamMaps(
					"high_pu_ls::select",
					promise.value(), request
			);

			stream_tuple.getStream()
					// TAKE LUMISECTION
					.map((Map record) -> (int) record.get("LUMISECTION"))
					// PROCESS LUMISECTION
					.forEach(lumisection -> {
						int from_ls = ls_interval[0];
						int to_ls = ls_interval[1];

						if (from_ls == -1) {
							ls_interval[0] = lumisection;
						} else if (to_ls == -1 || (lumisection - to_ls) == 1) {
							ls_interval[1] = lumisection;
						} else {
							if (ls_interval[0] != ls_interval[1] && ls_interval[1] != -1) {
								ls_intervals.add(list(
										ls_interval[0], ls_interval[1]
								));
							}

							ls_interval[0] = lumisection;
							ls_interval[1] = -1;
						}
					});

			// VALIDATE STREAM WAS SUCCESSFUL
			stream_tuple.getExceptionAnchor().throwIfPresent();

			if (!ls_intervals.isEmpty()) {
				synchronized (shared_context.mutex("final")) {
					runnumbers.put(arg_runnumber, ls_intervals);
				}
			}
		} catch (SQLException ex) {
			Logger.getLogger(HighPULSAutomat.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	@Override
	protected void done() {
		JSONObject high_pu_ls_json = new JSONObject(runnumbers);
		System.out.println(high_pu_ls_json.toString());

		try {
			String out_directory_path = "/home/omiguelc/Desktop/emtf_test";
			String directory_path = out_directory_path + "/high_pu";
			File directory = new File(directory_path);
			directory.mkdirs();

			String output_file = String.format("%s/%s.json", directory_path, "Cert_314472-324209_13TeV_PromptReco_Collisions18_JSON");

			try (PrintWriter fit_pw = new PrintWriter(new File(output_file))) {
				fit_pw.println(high_pu_ls_json.toString());
			}
		} catch (FileNotFoundException ex) {
			Logger.getLogger(HighPULSAutomat.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
