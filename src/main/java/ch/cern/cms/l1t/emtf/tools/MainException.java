/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.l1t.emtf.tools;

/**
 *
 * @author omiguelc
 */
public class MainException extends RuntimeException {

	/**
	 * Creates a new instance of <code>EMTFToolsException</code> without detail
	 * message.
	 */
	public MainException() {
	}

	/**
	 * Constructs an instance of <code>EMTFToolsException</code> with the
	 * specified detail message.
	 *
	 * @param msg the detail message.
	 */
	public MainException(String msg) {
		super(msg);
	}
}
