/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.l1t.emtf.tools.common.root;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;
import org.json.JSONObject;

/**
 *
 * @author omiguelc
 */
public class RootService {

	private ProcessBuilder builder;
	protected PrintWriter stdin;
	protected Scanner stdout;

	public static final String STOP_CODE = "STP";
	public static final String SERVICE_TASK_COMPLETE_CODE = "STC";
	public static final String SERVICE_TASK_RESPONSE_CODE = "STR";
	public static final String SERVICE_TASK_FAILED_CODE = "STF";

	public RootService(String exe_path) {
		builder = new ProcessBuilder(exe_path);
		builder.redirectError(ProcessBuilder.Redirect.INHERIT);
	}

	public void start() throws IOException {
		Process process = builder.start();

		OutputStream stdin = process.getOutputStream();
		this.stdin = new PrintWriter(stdin);

		InputStream stdout = process.getInputStream();
		this.stdout = new Scanner(stdout);
	}

	public RootServiceResponse send(JSONObject request_json) {
		stdin.println(request_json.toString());
		stdin.flush();

		String line;
		boolean read_json = false;
		JSONObject response_json = null;

		while ((line = stdout.nextLine()) != null) {
			if (read_json) {
				response_json = new JSONObject(line);
				read_json = false;
			} else if (null != line) {
				switch (line) {
					case SERVICE_TASK_COMPLETE_CODE:
						return new RootServiceResponse(true, response_json);
					case SERVICE_TASK_FAILED_CODE:
						return new RootServiceResponse(false, response_json);
					case SERVICE_TASK_RESPONSE_CODE:
						read_json = true;
						break;
					default:
						throw new RootServiceException("Unknown response.");
				}
			}
		}

		throw new RootServiceException("Response absent.");
	}

	public void stop() {
		stdin.println(STOP_CODE);
		stdin.flush();
	}

	public static class RootServiceResponse {

		private final boolean successful;
		private final JSONObject response;

		private RootServiceResponse(boolean successful, JSONObject response) {
			this.successful = successful;
			this.response = response;
		}

		public boolean isSuccessful() {
			return successful;
		}

		public JSONObject getResponse() {
			return response;
		}
	}

	public static class RootServiceException extends RuntimeException {

		public RootServiceException() {

		}

		public RootServiceException(String string) {
			super(string);
		}
	}
}
