/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.l1t.emtf.tools.automats.ugt_rate_vs_pu.fitters;

import ch.cern.cms.l1t.emtf.tools.common.fitters.Fit;
import ch.cern.cms.l1t.emtf.tools.common.fitters.RootFitter;
import java.util.Map;
import java.util.NoSuchElementException;
import org.json.JSONObject;

/**
 *
 * @author omiguelc
 */
public class RootXSecFitter extends RootFitter {

	public RootXSecFitter(String fitter_exec_path, Map<String, Double> parameters, double range_x_start, double range_x_stop) {
		super(fitter_exec_path, "[0] / x + [1] + [2] * x", parameters, 3, range_x_start, range_x_stop);
	}

	@Override
	public Fit newFit(JSONObject response) {
		return new Fit() {
			@Override
			public long getN() {
				return response.getLong("N");
			}

			@Override
			public double getParameter(int i) {
				String key = String.format("p%d", i);

				if (response.has(key)) {
					return response.getDouble(key);
				}

				throw new NoSuchElementException(String.format("Parameter %d doesn't exist", i));
			}

			@Override
			public double getParameterError(int i) {
				String key = String.format("p%d_err", i);

				if (response.has(key)) {
					return response.getDouble(key);
				}

				throw new NoSuchElementException(String.format("Parameter %d doesn't exist", i));
			}

			@Override
			public double getChiSqr() {
				return response.getDouble("chi2");
			}

			@Override
			public double predict(double x) {
				return getParameter(0) / x + getParameter(1) + getParameter(2) * x;
			}
		};
	}

}
