@Register ({
	"name": "collision_run_list::run_ls_count",

	"fields": {
		"COUNT": "integer"
	},

	"parameters": {
		"RUN": "integer"
	}
})

@Template
SELECT COUNT(*) "COUNT" FROM CMS_RUNTIME_LOGGER.LUMI_SECTIONS WHERE RUNNUMBER=:RUN