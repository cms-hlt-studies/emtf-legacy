/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.l1t.emtf.tools.common.fitters;

import java.util.List;

/**
 *
 * @author omiguelc
 */
public interface Fitter {

	public Fit fit(List<double[]> points);
}
