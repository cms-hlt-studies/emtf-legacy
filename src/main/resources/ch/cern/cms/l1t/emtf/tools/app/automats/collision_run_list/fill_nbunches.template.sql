@Register ({
	"name": "collision_run_list::fill_nbunches",

	"fields": {
		"NBUNCHES": "integer"
	},

	"parameters": {
		"FILL": "integer"
	}
})

@Template
SELECT NCOLLIDINGBUNCHES "NBUNCHES" FROM CMS_RUNTIME_LOGGER.RUNTIME_SUMMARY WHERE LHCFILL=:FILL