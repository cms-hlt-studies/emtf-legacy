/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.l1t.emtf.tools.common.root;

/**
 *
 * @author omiguelc
 */
public class Root {

	public static enum Color {
		// ENUM
		kWhite(0), kBlack(1), kGray(920),
		kRed(632), kGreen(416), kBlue(600), kYellow(400), kMagenta(616), kCyan(432),
		kOrange(800), kSpring(820), kTeal(840), kAzure(860), kViolet(880), kPink(900);

		// CLASS
		private int value;

		private Color(int value) {
			this.value = value;
		}

		public int value() {
			return value;
		}
	}
}
