/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.l1t.emtf.tools.common;

/**
 *
 * @author omiguelc
 */
public class DBScanParameters {

	private final double eps;
	private final int min_pts;

	public DBScanParameters(double eps, int min_pts) {
		this.eps = eps;
		this.min_pts = min_pts;
	}

	public double getEps() {
		return eps;
	}

	public int getMinPts() {
		return min_pts;
	}
}
