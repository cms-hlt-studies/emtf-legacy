/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.l1t.emtf.tools.automats.gt_halo;

import static ch.cern.cms.spartan.core.SpartanLang.*;
import ch.cern.cms.spartan.tools.concurrent.dispatchers.RequestDispatcher;
import ch.cern.cms.spartan.tools.concurrent.dispatchers.RequestDispatcher.SharedContext;
import ch.cern.cms.spartan.tools.database.connections.ConnectionShield;
import ch.cern.cms.spartan.tools.database.resultsets.ResultSetStreamTuple;
import ch.cern.cms.spartan.tools.database.snippets.TemplateSnippet;
import ch.cern.cms.spartan.tools.database.statements.PreparedStatementShield;
import ch.cern.cms.spartan.tools.database.statements.StatementTemplate;
import ch.cern.cms.spartan.tools.threading.pool.Loan;
import ch.cern.cms.spartan.util.lang.StringUtils;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

/**
 *
 * @author omiguelc
 */
public class GTHaloAutomat extends RequestDispatcher {

	@Override
	protected void handle(int worker_id, Map request, SharedContext shared_context) {
		List<Lumisection> pure_lumisections = new ArrayList();

		String runnumbers_str = (String) request.get("RUNNUMBERS");
		String algo_indexes_str = (String) request.get("ALGO_INDEXES");

		String[] runnumbers_str_arr = StringUtils.unpack(runnumbers_str, ",");
		String[] algo_indexes_str_arr = StringUtils.unpack(algo_indexes_str, ",");

		for (int i = 0; i < algo_indexes_str_arr.length; i++) {
			int runnumber = Integer.valueOf(runnumbers_str_arr[i]);
			int algo_index = Integer.valueOf(algo_indexes_str_arr[i]);

			Map subrequest = new HashMap(request);

			subrequest.remove("RUNNUMBERS");
			subrequest.remove("ALGO_INDEXES");

			subrequest.put("RUNNUMBER", runnumber);
			subrequest.put("ALGO_INDEX", algo_index);

			List<Lumisection> runnumber_pure_lumisections = getPureHaloLumisections(worker_id, subrequest);

			if (runnumber_pure_lumisections != null) {
				pure_lumisections.addAll(runnumber_pure_lumisections);
			}
		}

		SummaryStatistics pure_stats = bestStatistics(pure_lumisections);

		if (pure_stats != null) {
			if (pure_stats.getN() == 0) {
				return;
			}

			synchronized (shared_context.mutex("SUMMARY")) {
				System.out.println(String.format(
						"\"%s\",%d,\"%s\",\"%s\",\"%s\",%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%.16f,%d",
						request.get("ALGO"),
						request.get("LHCFILL"),
						request.get("STARTTIME"),
						request.get("STOPTIME"),
						request.get("INJECTIONSCHEME"),
						request.get("ENERGY"),
						request.get("PEAKPILEUP"),
						request.get("PEAKLUMI"),
						request.get("CROSSINGANGLE"),
						request.get("NCOLLIDINGBUNCHES"),
						pure_stats.getMean(),
						pure_stats.getStandardDeviation(),
						pure_stats.getMin(),
						pure_stats.getMax(),
						pure_stats.getN()
				));
			}
		}
	}

	private List<Lumisection> getPureHaloLumisections(int worker_id, Map subrequest) {
		try (Loan<ConnectionShield> promise = connection("CMS_RPC_R@adg")) {
			// PREPARE STATEMENT
			StatementTemplate statement_template = getStatementTemplate("gt_halo::select");
			statement_template.putSnippet("param_algo_index", new TemplateSnippet(String.format("%03d", subrequest.get("ALGO_INDEX")), null, null));
			PreparedStatementShield statement = statement_template.prepare();

			// RUN STATEMENT
			Map lag_record = new HashMap();
			List<Lumisection> pure_lumisections = new ArrayList();

			ResultSetStreamTuple<Map> stream_tuple = statement.queryAndStreamMaps(
					promise.value(), subrequest
			);

			stream_tuple.getStream()
					// LAG FIELDS
					.map(record -> {
						list(
								"BEAM_STABLE", "DELIVLUMISECTION", "DELIVLUMI",
								"INSTLUMI", "PILEUP", "RATE_HZ", "PRESCALE"
						).forEach((field) -> {
							Object value = record.get(field);
							Object previous_value = lag_record.getOrDefault(field, null);

							if (value == null) {
								record.put(field, previous_value);
							} else if (!Objects.equals(value, previous_value)) {
								lag_record.put(field, value);
							}
						});

						return record;
					})
					// REMOVE INCOMPLETE RECORDS
					.filter((Map record) -> {
						return Objects.equals(record.getOrDefault("LUMI_INFO", 0), 1)
								&& record.getOrDefault("BEAM_STABLE", null) != null
								&& record.getOrDefault("DELIVLUMISECTION", null) != null
								&& record.getOrDefault("DELIVLUMI", null) != null
								&& record.getOrDefault("INSTLUMI", null) != null
								&& record.getOrDefault("PILEUP", null) != null
								&& record.getOrDefault("RATE_HZ", null) != null
								&& record.getOrDefault("PRESCALE", null) != null;
					})
					// MAKE LUMISECTIONS
					.map(Lumisection::new)
					// ONLY BUFFER PURE LUMISECTIONS
					.forEach(lumisection -> {
						Category category = lumisection.classify();

						if (category.equals(Category.PURE)) {
							pure_lumisections.add(lumisection);
						}
					});

			// VALIDATE STREAM WAS SUCCESSFUL
			stream_tuple.getExceptionAnchor().throwIfPresent();

			return pure_lumisections;
		} catch (SQLException ex) {
			Logger.getLogger(GTHaloAutomat.class.getName()).log(Level.SEVERE, null, ex);
		}

		return null;
	}

	private SummaryStatistics bestStatistics(List<Lumisection> lumisections) {
		List<Lumisection> lumisections_copy = new ArrayList();

		lumisections_copy.addAll(lumisections);

		boolean retry = true;
		int count = lumisections_copy.size();
		while (retry && count > 1) {
			retry = false;

			SummaryStatistics stats = calcStatistics(lumisections_copy);
			double stddev = stats.getStandardDeviation() * 1;

			for (int i = 0; i < count; i++) {
				Lumisection lumisection = lumisections_copy.get(count - i - 1);

				double diff = lumisection.getRateBeforePrescale() - stats.getMean();

				if (Math.abs(diff) > stddev) {
					lumisections_copy.remove(lumisection);

					count--;
					i--;

					retry = true;
				}
			}
		}

		if (count == 0) {
			return null;
		}
		return calcStatistics(lumisections_copy);
	}

	private SummaryStatistics calcStatistics(List<Lumisection> lumisections) {
		SummaryStatistics rate_statistics = new SummaryStatistics();

		lumisections.forEach((lumisection) -> {
			rate_statistics.addValue(lumisection.getRateBeforePrescale());
		});

		return rate_statistics;
	}

	private static enum Category {
		PURE, PRE_COLLISION, COLLISION, UNKNOWN;
	}

	private static class Lumisection {

		private final boolean beam_stable;
		private final double delivlumisection;
		private final double delivlumi;
		private final double instlumi;
		private final double pileup;
		private final double rate;
		private final int prescale;

		public Lumisection(Map record) {
			beam_stable = ((int) record.get("BEAM_STABLE")) == 1;
			delivlumisection = (double) record.get("DELIVLUMISECTION");
			delivlumi = (double) record.get("DELIVLUMI");
			instlumi = (double) record.get("INSTLUMI");
			pileup = (double) record.get("PILEUP");
			rate = (double) record.get("RATE_HZ");
			prescale = (int) record.get("PRESCALE");
		}

		public Category classify() {
			if (!beam_stable && delivlumisection == 0 && delivlumi == 0 && instlumi < 10) {
				return Category.PURE;
			} else if (!beam_stable && delivlumisection == 0 && delivlumi == 0) {
				return Category.PRE_COLLISION;
			} else if (beam_stable && delivlumisection > 0 && delivlumi > 0) {
				return Category.COLLISION;
			}

			return Category.UNKNOWN;
		}

		public boolean isBeamStable() {
			return beam_stable;
		}

		public double getDelivlumisection() {
			return delivlumisection;
		}

		public double getDelivlumi() {
			return delivlumi;
		}

		public double getInstlumi() {
			return instlumi;
		}

		public double getPileup() {
			return pileup;
		}

		public double getRate() {
			return rate;
		}

		public double getRateBeforePrescale() {
			return rate * (double) prescale;
		}

		public int getPrescale() {
			return prescale;
		}
	}
}
