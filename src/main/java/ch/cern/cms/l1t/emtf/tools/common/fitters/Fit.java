/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.l1t.emtf.tools.common.fitters;

/**
 *
 * @author omiguelc
 */
public interface Fit {

	public long getN();

	public double getParameter(int i);

	public double getParameterError(int i);

	public double getChiSqr();

	public double predict(double x);
}
