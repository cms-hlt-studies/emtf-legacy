/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.l1t.emtf.tools.common.fitters;

import ch.cern.cms.l1t.emtf.tools.EMTFTools;
import ch.cern.cms.l1t.emtf.tools.common.root.RootService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author omiguelc
 */
public abstract class RootFitter extends RootService implements Fitter {

	private final String function;
	private final Map<String, Double> parameters;
	private final int parameter_count;
	private final double fit_from_x;
	private final double fit_to_x;

	public RootFitter(String exec_path, String function, Map<String, Double> parameters, int parameter_count, double fit_from_x, double fit_to_x) {
		super(exec_path);

		this.function = function;
		this.parameters = parameters;
		this.parameter_count = parameter_count;
		this.fit_from_x = fit_from_x;
		this.fit_to_x = fit_to_x;
	}

	@Override
	public void start() throws IOException {
		super.start();

		// READ SET_FUNCTION TEMPLATE
		JSONObject request = EMTFTools.readJSONTemplate("templates/fits/set_function.json");

		// FILL IN SET_FUNCTION TEMPLATE
		request.put("function", function);
		request.put("fit_from_x", fit_from_x);
		request.put("fit_to_x", fit_to_x);

		request.put("parameter_count", parameter_count);
		parameters.forEach(request::put);

		// SEND
		send(request);
	}

	@Override
	public Fit fit(List<double[]> points) {
		try {
			// READ BODY TEMPLATE
			JSONObject request = EMTFTools.readJSONTemplate("templates/fits/fit.json");

			List<Double> x_arr = new ArrayList();
			List<Double> y_arr = new ArrayList();

			for (double[] point : points) {
				x_arr.add(point[0]);
				y_arr.add(point[1]);
			}

			// FILL IN FIT TEMPLATE
			request.put("x_arr", x_arr);
			request.put("y_arr", y_arr);

			// SEND
			RootServiceResponse response = send(request);

			// UNPACK
			JSONObject response_json = response.getResponse();

			// RETURN
			if (response.getResponse().getBoolean("successful")) {
				return newFit(response_json);
			}
		} catch (IOException ex) {
			Logger.getLogger(RootFitter.class.getName()).log(Level.SEVERE, null, ex);
		}

		return null;
	}

	public abstract Fit newFit(JSONObject response);

}
