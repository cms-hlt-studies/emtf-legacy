/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.cern.cms.l1t.emtf.tools;

import ch.cern.cms.l1t.emtf.tools.automats.collision_run_list.CollisionRunListAutomat;
import ch.cern.cms.l1t.emtf.tools.automats.gt_halo.GTHaloAutomat;
import ch.cern.cms.l1t.emtf.tools.automats.high_pu_lumisections.HighPULSAutomat;
import ch.cern.cms.l1t.emtf.tools.automats.ugt_halo.UGTHaloAutomat;
import ch.cern.cms.l1t.emtf.tools.automats.ugt_rate_vs_pu.UGTRateVsPUDBScanAutomat;
import ch.cern.cms.l1t.emtf.tools.automats.ugt_rate_vs_pu.UGTRateVsPUFitterAutomat;
import ch.cern.cms.l1t.emtf.tools.automats.ugt_rate_vs_pu.fitters.RootRateFitter;
import ch.cern.cms.l1t.emtf.tools.automats.ugt_rate_vs_pu.fitters.RootXSecFitter;
import ch.cern.cms.l1t.emtf.tools.common.DBScanParameters;
import ch.cern.cms.l1t.emtf.tools.common.fitters.Fitter;
import ch.cern.cms.l1t.emtf.tools.common.plotters.RootPlotter;
import ch.cern.cms.l1t.emtf.tools.common.root.Root;
import ch.cern.cms.spartan.commons.database.connections.ConnectionShield;
import ch.cern.cms.spartan.commons.database.snippets.TemplateSnippet;
import ch.cern.cms.spartan.commons.database.statements.PreparedStatementShield;
import ch.cern.cms.spartan.commons.database.statements.StatementTemplate;
import ch.cern.cms.spartan.commons.text.TextUtils;
import ch.cern.cms.spartan.commons.threading.pool.Loan;
import ch.cern.cms.spartan.core.Spartan;
import static ch.cern.cms.spartan.core.SpartanLang.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.json.JSONObject;

/**
 *
 * @author omiguelc
 */
public class EMTFTools {

	public static String OUTPUT_DIRECTORY;

	public static void main(String[] args) {
		try {
			if (args.length == 0) {
				throw new MainException("Missing config directory argument");
			} else if (Objects.isNull(args[0])) {
				throw new MainException("Config directory argument is null");
			}

			OUTPUT_DIRECTORY = "/home/omiguelc/Desktop/emtf_outputs";

			Spartan.init(args[0]);
		} catch (MainException ex) {
			Logger.getLogger(EMTFTools.class.getName()).log(Level.SEVERE, null, ex);
		}

		run();
	}

	public static void run() {
		// PARAMETERS
		// DBScanParameters db_params = new DBScanParameters(0.002, 5);
		DBScanParameters dbscan_params = new DBScanParameters(0.005, 5);

		List<String> algos = list(
				//				"L1_SingleMu0_EMTF",
				//				"L1_SingleMuCosmics_EMTF",
				//				"L1_SingleMu12_LowQ_EMTF",
				//				"L1_SingleMu12_DQ_EMTF",
				//				"L1_SingleMu22_EMTF"
				"L1_SingleMu0_BMTF",
				"L1_SingleMu0_OMTF",
				"L1_SingleMu22_BMTF",
				"L1_SingleMu22_OMTF",
				"L1_SingleMu22"
		//
		//				"L1_SingleEG28er1p5",
		//				"L1_SingleEG28_FWD2p5",
		//				"L1_ETMHF100",
		//
		//				"L1_SingleMu22_BMTF",
		//				"L1_SingleMu22_OMTF",
		//				"L1_DoubleMu_15_7",
		//				"L1_SingleEG28er2p5",
		//				"L1_SingleLooseIsoEG28er2p5",
		//				"L1_SingleJet160er2p5",
		//				"L1_DoubleIsoTau32er2p1"
		);

		// ALL
		// dumpAllRates(algos, dbscan_params);
		// fitNormRates(algos, dbscan_params);
		// fitXSec(algos, dbscan_params);
		// makeHLLHCRateFiles(algos);
		// plotNormRateSummaries(algos);
		// plotXSecSummaries(algos);
		// plotNormRateFit(algos);
		plotHLLHCRates(algos);
	}

	// ALL
	public static void dumpAllRates(List<String> algos, DBScanParameters dbscan_parameters) {
		System.out.println("Performing DBSCAN");
		runUGTRateVsPUAutomat(UGTRateVsPUDBScanAutomat.Field.NORM_RATE,
				dbscan_parameters,
				(String[]) algos.toArray(new String[algos.size()])
		);
	}

	public static void fitNormRates(List<String> algos, DBScanParameters dbscan_parameters) {
		algos.forEach((String algo) -> {
			try {
				System.out.println("Fitting " + algo);

				// START SERVICE
				RootRateFitter fitter = new RootRateFitter(
						"/home/omiguelc/Work/CERN/CMS/L1T/EMTF/development/emtf-root-fitter/bin/run",
						map(
								"p0", 0d,
								"p1", 0d,
								"p2", 0d
						),
						10,
						110
				);
				fitter.start();

				// FIT
				runUGTRateVsPUXFitAutomat(
						algo,
						UGTRateVsPUFitterAutomat.Field.NORM_RATE,
						fitter,
						dbscan_parameters,
						algo
				);

				// STOP SERVICE
				fitter.stop();
			} catch (IOException ex) {
				Logger.getLogger(EMTFTools.class.getName()).log(Level.SEVERE, null, ex);
			}
		});
	}

	public static void fitXSec(List<String> algos, DBScanParameters dbscan_parameters) {
		algos.forEach((String algo) -> {
			try {
				System.out.println("Fitting " + algo);

				// START SERVICE
				RootXSecFitter fitter = new RootXSecFitter(
						"/home/omiguelc/Work/CERN/CMS/L1T/EMTF/development/emtf-root-fitter/bin/run",
						map(
								"p0", 0d,
								"p1", 0d,
								"p2", 0d
						),
						10,
						110
				);
				fitter.start();

				// FIT
				runUGTRateVsPUXFitAutomat(
						algo,
						UGTRateVsPUFitterAutomat.Field.XSEC,
						fitter,
						dbscan_parameters,
						algo
				);

				// STOP SERVICE
				fitter.stop();
			} catch (IOException ex) {
				Logger.getLogger(EMTFTools.class.getName()).log(Level.SEVERE, null, ex);
			}
		});
	}

	public static void makeHLLHCRateFiles(List<String> algos) {
		algos.forEach((algo) -> {
			try {
				String algo_lhcfill_7334 = "/home/omiguelc/Desktop/emtf_outputs/ugt_rate_vs_pu/norm_rate/data/" + algo + "/7334.csv";
				String algo_lhcfill_7358 = "/home/omiguelc/Desktop/emtf_outputs/ugt_rate_vs_pu/norm_rate/data/" + algo + "/7358.csv";

				Scanner reader_7334 = new Scanner(new File(algo_lhcfill_7334));
				Scanner reader_7358 = new Scanner(new File(algo_lhcfill_7358));

				PrintWriter fit_pw = new PrintWriter(new File("/home/omiguelc/Desktop/emtf_outputs/ugt_rate_vs_pu/norm_rate/data/" + algo + "/high_pu.csv"));

				reader_7334.forEachRemaining(fit_pw::println);
				reader_7358.forEachRemaining(fit_pw::println);

				fit_pw.close();
				reader_7334.close();
				reader_7358.close();
			} catch (FileNotFoundException ex) {
				// DO NOTHING
			}
		});
	}

	public static void plotNormRateSummaries(List<String> algos) {
		try {
			// SET OUPUT DIR
			String base_output_dir = EMTFTools.OUTPUT_DIRECTORY + "/plots/norm_rate";

			// START SEVICE
			RootPlotter plotter = new RootPlotter("/home/omiguelc/Work/CERN/CMS/L1T/EMTF/development/emtf-root-plotter/bin/run");
			plotter.start();

			// PLOT PARAMETERS IN TIME
			algos.forEach(algo -> {
				for (int i = 0; i < 3; i++) {
					try {
						// READ BODY TEMPLATE
						JSONObject json = readJSONTemplate("modules/ugt_rate_vs_pu/json/plots/parameters_vs_time/body.json");

						// MAKE OUTPUT DIR
						String algo_output_dir_path = base_output_dir + "/summaries/" + algo;
						File algo_output_dir = new File(algo_output_dir_path);
						algo_output_dir.mkdirs();

						// NAME
						String name = String.format("p%d_vs_time", i);

						// FILL IN BODY TEMPLATE
						json.put("output_file", String.format("%s/%s.root", algo_output_dir, name));

						json.put("name", name);
						json.put("title", algo);

						json.put("y_axis_title", "P" + i);

						// READ ENTRY TEMPLATE
						JSONObject entry_json = readJSONTemplate("modules/ugt_rate_vs_pu/json/plots/parameters_vs_time/entry.json");

						// FILL IN ENTRY TEMPLATE
						entry_json.put("file", String.format("/home/omiguelc/Desktop/emtf_outputs/ugt_rate_vs_pu/norm_rate/summaries/%s.csv", algo));

						entry_json.put("legend", algo);
						entry_json.put("color", Root.Color.kRed.value());

						entry_json.put("value_index", 14 + i * 2);
						entry_json.put("value_normalize", false);

						// ADD ENTRY TO BODY TEMPLATE
						json.append("entries", entry_json);

						// SEND
						plotter.send(json);
					} catch (IOException ex) {
						Logger.getLogger(EMTFTools.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			});

			// STOP SERVICE
			plotter.stop();
		} catch (IOException ex) {
			Logger.getLogger(EMTFTools.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static void plotXSecSummaries(List<String> algos) {
		try {
			// SET OUPUT DIR
			String base_output_dir = EMTFTools.OUTPUT_DIRECTORY + "/plots/xsec";

			// START SEVICE
			RootPlotter plotter = new RootPlotter("/home/omiguelc/Work/CERN/CMS/L1T/EMTF/development/emtf-root-plotter/bin/run");
			plotter.start();

			// PLOT PARAMETERS IN TIME
			algos.forEach(algo -> {
				for (int i = 0; i < 3; i++) {
					try {
						// READ BODY TEMPLATE
						JSONObject json = readJSONTemplate("modules/ugt_rate_vs_pu/json/plots/parameters_vs_time/body.json");

						// MAKE OUTPUT DIR
						String algo_output_dir_path = base_output_dir + "/summaries/" + algo;
						File algo_output_dir = new File(algo_output_dir_path);
						algo_output_dir.mkdirs();

						// NAME
						String name = String.format("p%d_vs_time", i);

						// FILL IN BODY TEMPLATE
						json.put("output_file", String.format("%s/%s.root", algo_output_dir, name));

						json.put("name", name);
						json.put("title", algo);

						json.put("y_axis_title", "P" + i);

						// READ ENTRY TEMPLATE
						JSONObject entry_json = readJSONTemplate("modules/ugt_rate_vs_pu/json/plots/parameters_vs_time/entry.json");

						// FILL IN ENTRY TEMPLATE
						entry_json.put("file", String.format("/home/omiguelc/Desktop/emtf_outputs/ugt_rate_vs_pu/xsec/summaries/%s.csv", algo));

						entry_json.put("legend", algo);
						entry_json.put("color", Root.Color.kRed.value());

						entry_json.put("value_index", 14 + i * 2);
						entry_json.put("value_normalize", false);

						// ADD ENTRY TO BODY TEMPLATE
						json.append("entries", entry_json);

						// SEND
						plotter.send(json);
					} catch (IOException ex) {
						Logger.getLogger(EMTFTools.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			});

			// STOP SERVICE
			plotter.stop();
		} catch (IOException ex) {
			Logger.getLogger(EMTFTools.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static void plotNormRateFit(List<String> algos) {
		try {
			// SET OUPUT DIR
			String base_output_dir = EMTFTools.OUTPUT_DIRECTORY + "/plots/norm_rate";

			// START SEVICE
			RootPlotter plotter = new RootPlotter("/home/omiguelc/Work/CERN/CMS/L1T/EMTF/development/emtf-root-plotter/bin/run");
			plotter.start();

			// PLOT FITS
			algos.forEach(algo -> {
				try {
					String summary_path = String.format("/home/omiguelc/Desktop/emtf_outputs/ugt_rate_vs_pu/norm_rate/summaries/%s.csv", algo);
					Reader summary_reader = new FileReader(summary_path);

					Iterable<CSVRecord> summary_it = CSVFormat.DEFAULT.parse(summary_reader);

					for (CSVRecord summary_record : summary_it) {
						int lhcfill = asInt(summary_record.get(2));
						double p0 = asDouble(summary_record.get(14));
						double p1 = asDouble(summary_record.get(16));
						double p2 = asDouble(summary_record.get(18));

						// READ BODY TEMPLATE
						JSONObject json = readJSONTemplate("modules/ugt_rate_vs_pu/json/plots/rate_vs_pu/body.json");

						// MAKE OUTPUT DIR
						String algo_output_dir_path = base_output_dir + "/fits/" + algo;
						File algo_output_dir = new File(algo_output_dir_path);
						algo_output_dir.mkdirs();

						// NAME
						String name = String.format("rate_vs_pu_fill_%d", lhcfill);

						// FILL IN BODY TEMPLATE
						json.put("output_file", String.format("%s/%s.png", algo_output_dir, name));

						json.put("name", name);
						json.put("title", String.format("%s Fill %d", algo, lhcfill));

						json.put("y_axis_title", "Rate / # Colliding Bunches [Hz]");

						// READ ENTRY TEMPLATE
						JSONObject entry_json = readJSONTemplate("modules/ugt_rate_vs_pu/json/plots/rate_vs_pu/entry.json");

						// FILL IN ENTRY TEMPLATE
						entry_json.put("file", "/home/omiguelc/Desktop/emtf_outputs/ugt_rate_vs_pu/norm_rate/data/" + algo + "/" + lhcfill + ".csv");

						entry_json.put("legend", Integer.toString(lhcfill));

						entry_json.put("color", Root.Color.kBlue.value());
						entry_json.put("noise_color", Root.Color.kBlack.value());
						entry_json.put("fit_color", Root.Color.kRed.value());
						entry_json.put("linear_fit_color", Root.Color.kMagenta.value());

						entry_json.put("function", "[0] + [1] * x + [2] * x^2");
						entry_json.put("parameter_count", 3);
						entry_json.put("fit_from_x", 0);
						entry_json.put("fit_to_x", 110);

						entry_json.put("value_normalize_to", 1);

						entry_json.put("p0", p0);
						entry_json.put("p1", p1);
						entry_json.put("p2", p2);

						// ADD ENTRY TO BODY TEMPLATE
						json.append("entries", entry_json);

						// SEND
						plotter.send(json);
					}
				} catch (IOException ex) {
					Logger.getLogger(EMTFTools.class.getName()).log(Level.SEVERE, null, ex);
				}
			});

			// STOP SERVICE
			plotter.stop();
		} catch (IOException ex) {
			Logger.getLogger(EMTFTools.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static void plotHLLHCRates(List<String> algos) {
		algos.forEach((algo -> {
			try {
				// SET OUPUT DIR
				String base_output_dir = EMTFTools.OUTPUT_DIRECTORY + "/plots/norm_rate/hllhc";

				// START SEVICE
				RootPlotter plotter = new RootPlotter("/home/omiguelc/Work/CERN/CMS/L1T/EMTF/development/emtf-root-plotter/bin/run");
				plotter.start();

				// READ BODY TEMPLATE
				JSONObject json = readJSONTemplate("modules/ugt_rate_vs_pu/json/plots/rate_vs_pu_hl_lhc/body.json");

				// MAKE OUTPUT DIR
				String algo_output_dir_path = base_output_dir;
				File algo_output_dir = new File(algo_output_dir_path);
				algo_output_dir.mkdirs();

				// FILL IN BODY TEMPLATE
				json.put("output_file", String.format("%s/%s.png", algo_output_dir, algo));

				json.put("name", "rate_vs_pu_fill_high_pu");
				json.put("title", String.format("%s Fills %d and %d", algo, 7334, 7358));

				json.put("y_axis_title", "Rate / # Colliding Bunches [Hz]");

				// READ ENTRY TEMPLATE
				JSONObject entry_json = readJSONTemplate("modules/ugt_rate_vs_pu/json/plots/rate_vs_pu_hl_lhc/entry.json");

				// FILL IN ENTRY TEMPLATE
				entry_json.put("file", "/home/omiguelc/Desktop/emtf_outputs/ugt_rate_vs_pu/norm_rate/data/" + algo + "/high_pu.csv");

				entry_json.put("legend", "High Pile-up");

				entry_json.put("color", Root.Color.kBlue.value());
				entry_json.put("noise_color", Root.Color.kBlack.value());
				entry_json.put("fit_color", Root.Color.kRed.value());
				entry_json.put("linear_fit_color", Root.Color.kMagenta.value());

				entry_json.put("function", "[0] + [1] * x + [2] * x^2");
				entry_json.put("parameter_count", 3);
				entry_json.put("fit_from_x", 0);
				entry_json.put("fit_to_x", 300);

				entry_json.put("value_normalize_to", 1);

				entry_json.put("p0", 0);
				entry_json.put("p1", 0);
				entry_json.put("p2", 0);

				// ADD ENTRY TO BODY TEMPLATE
				json.append("entries", entry_json);

				// SEND
				plotter.send(json);
			} catch (IOException ex) {
				Logger.getLogger(EMTFTools.class.getName()).log(Level.SEVERE, null, ex);
			}
		}));
	}

	// COMMANDS
	public static void runCollisionRunList() {
		String request_file = "/home/omiguelc/Downloads/requests.csv";

		try (Reader in = new FileReader(request_file)) {
			Iterable<CSVRecord> records = CSVFormat.DEFAULT.withHeader(
					"ERA", "FILL", "RUN", "nBunch", "Total LS", "Phys LS", "DCS LS", "Gold LS", "L1muon", "L1calo", "RR Comments"
			).parse(in);

			CollisionRunListAutomat automat = new CollisionRunListAutomat();

			boolean first = true;
			for (CSVRecord record : records) {
				if (first) {
					first = false;
					continue;
				}

				automat.addRequest(record.toMap());
			}

			automat.run();
		} catch (IOException ex) {
			Logger.getLogger(EMTFTools.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static void runHighPULumisectionsAutomat() {
		try {
			List<Map> requests = new ArrayList();
			URL url = new URL("https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions18/13TeV/PromptReco/Cert_314472-324209_13TeV_PromptReco_Collisions18_JSON.txt");
			HttpsURLConnection url_connection = (HttpsURLConnection) url.openConnection(); // Cast shouldn't fail
			HttpsURLConnection.setFollowRedirects(true);

			InputStream input_stream = url_connection.getInputStream();
			JSONObject ls_json = JSONUtils.readJSON(input_stream);

			Map cert_runnumbers = JSONUtils.javafy(ls_json);
			cert_runnumbers.forEach((runnumber, ls_pairs) -> {
				List ls_pairs_list = (List) ls_pairs;

				((List) ls_pairs).forEach(ls_pair -> {
					int from_ls = (int) ((List) ls_pair).get(0);
					int to_ls = (int) ((List) ls_pair).get(1);

					requests.add(map(
							"RUNNUMBER", runnumber,
							"FROM_LS", from_ls,
							"TO_LS", to_ls
					));
				});
			});

			HighPULSAutomat automat = new HighPULSAutomat();
			requests.forEach(automat::addRequest);
			automat.run();
		} catch (MalformedURLException ex) {
			Logger.getLogger(EMTFTools.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(EMTFTools.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static void runGTHaloAutomat(String... algos) {
		try (Loan<ConnectionShield> promise = connection("CMS_RPC_R@adg")) {
			// PREPARE STATEMENT
			StatementTemplate statement_template = getStatementTemplate("gt_halo::feed");
			statement_template.putSnippet("algos", new TemplateSnippet(tokenizeAlgos(algos), null, null));
			PreparedStatementShield statement = statement_template.prepare();

			// NEW AUTOMAT
			GTHaloAutomat automat = new GTHaloAutomat();
			statement.queryAndGetMaps(promise.value()).forEach(automat::addRequest);
			automat.run();
		} catch (SQLException ex) {
			Logger.getLogger(EMTFTools.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static void runUGTHaloAutomat(String... algos) {
		try (Loan<ConnectionShield> promise = connection("CMS_RPC_R@adg")) {
			// PREPARE STATEMENT
			StatementTemplate statement_template = getStatementTemplate("ugt_halo::feed");
			statement_template.putSnippet("algos", new TemplateSnippet(tokenizeAlgos(algos), null, null));
			PreparedStatementShield statement = statement_template.prepare();

			// NEW AUTOMAT
			UGTHaloAutomat automat = new UGTHaloAutomat();
			statement.queryAndGetMaps(promise.value()).forEach(automat::addRequest);
			automat.run();
		} catch (SQLException ex) {
			Logger.getLogger(EMTFTools.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static void runUGTRateVsPUAutomat(UGTRateVsPUDBScanAutomat.Field field, DBScanParameters dbscan_parameters, String... algos) {
		try (Loan<ConnectionShield> promise = connection("CMS_RPC_R@adg")) {
			// PREPARE STATEMENT
			StatementTemplate statement_template = getStatementTemplate("ugt_rate_vs_pu::feed");
			statement_template.putSnippet("algos", new TemplateSnippet(tokenizeAlgos(algos), null, null));
			PreparedStatementShield statement = statement_template.prepare();

			// CREATE DISPATCHER
			UGTRateVsPUDBScanAutomat automat = new UGTRateVsPUDBScanAutomat(field, dbscan_parameters);
			statement.queryAndGetMaps(promise.value()).forEach(automat::addRequest);
			automat.run();
		} catch (SQLException ex) {
			Logger.getLogger(EMTFTools.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static void runUGTRateVsPUXFitAutomat(String output_file_name, UGTRateVsPUFitterAutomat.Field field, Fitter fitter, DBScanParameters dbscan_parameters, String... algos) {
		try (Loan<ConnectionShield> promise = connection("CMS_RPC_R@adg")) {
			// PREPARE STATEMENT
			StatementTemplate statement_template = getStatementTemplate("ugt_rate_vs_pu::feed");
			statement_template.putSnippet("algos", new TemplateSnippet(tokenizeAlgos(algos), null, null));
			PreparedStatementShield statement = statement_template.prepare();

			// NEW AUTOMAT
			UGTRateVsPUFitterAutomat automat = new UGTRateVsPUFitterAutomat(output_file_name, field, fitter, dbscan_parameters);
			statement.queryAndGetMaps(promise.value()).forEach(automat::addRequest);
			automat.run();
		} catch (SQLException ex) {
			Logger.getLogger(EMTFTools.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	// HELPERS
	public static String tokenizeAlgos(String... algos) {
		StringBuilder algos_sql_sb = new StringBuilder();

		int last_index = algos.length - 1;
		for (int i = 0; i < algos.length; i++) {
			algos_sql_sb
					.append(String.format("'%s'", algos[i]))
					.append((i != last_index) ? "," : "");
		}

		return algos_sql_sb.toString();
	}

	public static JSONObject readJSONTemplate(String path) throws IOException {
		String content = TextUtils.readInputStream(ris(path));
		return new JSONObject(content);
	}

}
